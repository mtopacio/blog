---
title: "Project Management: Intro"
date: 2020-10-01T10:08:27-07:00
author: Mark Topacio
categories: ["flask","python","project management"]
draft: true 
---

This marks the beginning of a full-length project I'm undertaking. I'm going to be trying to create a minimal _Flask_ app designed around project management. When looking through my open-source options, there was a lot of good candidates like [OpenProject]() and [Redmine](); however, I wanted something even simpler and streamlined.

## Project Requirements

What I wanted was the ability to add projects and display its progress as what css frameworks call steps. I also want to be able to switch displays to a Kanban style layout and organize each entry based on status. These are the functional requirements that first come to mind. Others can be added as necessary. It isn't a far step that what's already available.  

There are a few other technical requirements that steered my away from available applications. I know I have _Python_ installed at the computer at work. Right of the bat, I know this is what I want to use. I may have admin rights on the computer; however, it is shared and I'm worried someone might make noise about me installing more unneeded programs. My position isn't that of an engineer, and all of this is outside current practices. All this leads to it being written mostly in _Python_. Another factor preventing the use out of available programs, especially those cloud-based, is the protection of data. I don't think any of the what I would be writing about is sensitive data; however, I'd like to avoid that mess altogether if possible. 

_SQLite_ comes with _Python_, so this will be used as my RDMS. Data-types are at a minimal; however, it addresses everything I will be needing.

I also want to use the browser to provide my main GUI. This brings me to [_Flask_](https://flask.palletsprojects.com/en/1.1.x/#). I might also want to use this on other computers, or have a co-worker use it on their phone. Web-hosted apps serve the quickest route to achieve this versus trying to take into account everyone's computer/phone software. in addition, _Flask_ has a number of modules to import that can add functionality like user authentication.

Since I'm using _Flask_, I thought I'd also try my hand at _Dash_/_Plotly_. This is an alternative to _Bokeh_; however, _Bokeh_ uses _Tornado_ as its web server. _Dash_ uses _Flask_, which we already have. I still don't know how I want to use this, but I know I will later on for some data visualization like Gantt charts or maybe some productivity histograms.

The only other thing I can think of at the moment is CSS file. I'm still very green in terms of front-end development, so I don't really want to create CSS styles from scratch. One framework I always turn to is [Spectre.css](). It provides an incredible amount lot of functionality in a minimal footprint of ~10kb. It also has a minimal learning curve and is still very current. The last commit was a month or two ago at the time of writing. 

That's it. _Python_, _SQLite_, _Flask_, _Dash_, and _spectre.css_. Those are its dependencies. I'm trying to be as minimal as possible.

## Virtual Environment

First things first, let's set up a virtual environment to work in and manage dependencies. I also exported the requirements for reference. 

```bash
$ virtualenv venv
$ source venv/bin/activate
$ pip install flask dash
$ pip freeze > requirements.txt
```

## Flask

Let's start with trying _Flask_. I'm just trying their [quickstart](https://flask.palletsprojects.com/en/1.1.x/quickstart/) example to make sure everything works.

Create an `app.py` file with the following:
```python
from flask import Flask
app = Flask(__nam__)particluar

@app.route('/')
def hello_world():
    return 'Hello World!'
```

Then run:
```bash
$ export FLASK_APP=app.py
$ flask run
```

Hopefully, everything runs smooth. _Flask_ lets you know it's serving your app on your local host through port 5000. If you type `http://localhost:5000` or `http://127.0.0.1:5000`, you should arrive at a white page with the text'Hello World!'. 

Easy. We crossed the first hurdle. Mind you this is also only accessible to you on your computer. To make it externally visible, you need to run `flask run --host=0.0.0.0`. Now we're going to customize that page with a whole lot of HTML/_Python_ to come up with our app. _Flask_ also throws out a disclaimer to not use the built-server for production. It may only be for development, but it's good enough for my purposes.

## Creating the Web App

First, I'm going to download `spectre.css` and host it locally to try and help performance. These files aren't that big, so I don't think it should be that much of a difference, but one of my pet peeves with contemporary web development is the amount of imports that take place in the background just to get a page running. One of the plug-ins I have on _Firefox_ prevents this without your authorization. In order to provide service, some sites rely on a _ridiculous_ number of outside pages. So grab that latest release, extract the relevant files, and save it alongside your project

```bash
$ mkdir static && cd static
$ wget https://github.com/picturepan2/spectre/archive/v0.5.9.tar.gz 
$ tar xvf v0.5.9.tar.gz
# folder spectre-0.5.9 is extracted
$ cp spectre-0.5.9/dist/*.min.css .
$ rm v0.5.9.tar.gz && rm -rf spectre-0.5.9
```

The majority of the extracted files belong to the file's documentation. According to the same documentation, you only need three files: `spectre.min.css`, `spectre-icons.min.css`, and `spectre-exp.min.css`. If you want, you can delete the rest after copying as I did.

All of my project's components are in order. Now comes the grunt work. A lot of this involves trial-and-error for me, especially the front end process, so it might take a while. This project will span multiple posts which I'll divide into three sections in no particular order reflective of the dependencies we've already installed. The first is this intro. The second will be the actual writing of the _Flask_ app. This also includes the front-end work. The last of which will deal with database design.

Updates will also come in the form of edits per each post, so stay tuned. 
