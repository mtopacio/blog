---
title: "Introduction to Bash: Part I"
date: 2020-10-18T14:02:57-07:00
author: Mark Topacio
categories: ["bash","linux"]
draft: false
---

This will be my little into to bash for anyone that needs it. It's broken up into two sections. The first of which is limited to just the basic commands and text manipulation. If you need to learn to use the terminal for a class or for work, this should be all you need to get through it. The second section will deal more with scripting. You may not need to learn it, but it'll make your life a lot easier, so don't skip it. 

If at anytime you need help with something, you can throw in the option `--help`. _Options_ are the little arguments you can append to the end of a command to specify certain functionality. Most programs include some time of help, so `$ command --help` should give you some guidance on how to use it. Most user-friendly distributions also have `man` built in, which provides the manual on how to use a command.

---

##  Basics

These commands help you figure out how to move around once you have access to a terminal. You should also be able to manipulate the files you're working with. 

| Command | Description                                       |
| ------- | ------------------------------------------------- |
| ls      | lists the files in your current directory         |
| cd      | change directory                                  |
| pwd     | print working (i.e. current) directory            |
| mv      | moves a file                                      |
| cp      | copies a file                                     |
| rm      | removes a file                                    |
| mkdir   | make a directory                                  |
| rmdir   | removes a directory                               |
| touch   | create a file without any content                 |
| find    | helps locate files and perform operations on them |
| whoami  | prints your current username                      |
| uname   | prints system information                         |

For a few of these, I generally use them with an option or two for my purposes. 

`ls` is a loaded one. It will generally be used with `-alh`. `-a` specifies to list all the files, including the hidden ones. These are prefixed with a period (i.e `.`). Withing your home directory, there should be [`.bashrc`](https://mtopacio.gitlab.io/blog/posts/bashrc/) which is included in most systems. This file allows you to add some specifications and aliases to your session. With a straight `ls`, it will not be listed; however, if you add on that option, it will. `-l` formats all the output into a list, which includes a column for them each file uses. `-h` lists these files in a human-readable format. Note that human-readable format is in bytes. You can also add the option `-R` to list everything within a directory recursively, meaning it will also list the files inside directories. Default is to just list directories. Also, notice how I chained together all these options. Instead of typing `ls -a -l -h`, you can simplify it to `-ls -alh`. You can also add a destination if you want to list the contents of a specific directory.

`ls -l` also gives you a couple of other things on the left-hand side. Modes make up the first column, and it's made of 10 characters. Modes designate permissions for that file and are also addressed later. The first character will always be a `-` or `d`. This stands for a plain file or a directory. The following nine can be split up into three groups relating to _user_, _group_, and _other_ permissions. Each of the three sections will have three spaces for those modes (i.e. `rwx`). So, if you were to see `-rwx------`, you would know that it is a file, not a directory, and it can be read, written to, and executed by only you. (mind you _superusers_ can override all of these). The first section (i.e. the second space to the fourth) correspond to the user. The next three space after that correspond to the group. The last three spaces correspond to anyone else that may come across that file. The next column after the modes are the number of files that link to it. You can table this for now since this won't be of much use. The next column is the user, followed by the group, the space it takes up, the date and time it was edited, and the name.

The command line also accepts __wildcards__. These are either the asterisk (i.e. `*`), the question mark (i.e. `?`), or any characters specified in hard brackets (i.e. []). The question mark is limited to matching any singular character, and the asterisk will replace one or more matches. Let's take a directory with the files `file1.txt`, `file2.txt`, `file3.txt`. `ls file?.txt` and `ls file*.txt`will both list all of them since there's only a one character difference between them. `ls file[1,2].txt` will only list `file1.txt` and `file2.txt`. Using `ls *` also has the same effect as running `ls -R`.

With `cp` and `mv`, a source and destination is required. If a destination filename is omitted, the default action is to take the name of the original file. `mv` can also be used to rename a file. A single period (`.`) normally references the current directory. You can use `..` to specify the directory above you. Notice when you use `ls -a`, `.` and `..` are two of the items returned.

If `rm` is used on a directory, it will throw an error. You can add the option `-r` for it to act recursively.  Adding on the option `-f` will preemptively answer yes for any confirmations that come up. 

`find` is also another loaded command. I generally use it for a quick search of files. You should use is as `find -name [filename]`. You can also limit your searches based on other criteria. Take a look at `find --help` for all of them.

`uname` by default justs lists the kernel. That alone just returns _Linux_, which isn't all that helpful to most. Use it with `-a` to return all the information.

<script id="asciicast-366144" src="https://asciinema.org/a/366144.js" async></script>

---

## Explore a file, piping, and redirections

Once you can figure out where you are and move around files, you can start messing around with them. 

| Command | Description                                           |
| ------- | ----------------------------------------------------- |
| cat     | prints out the contents of a file                     |
| head    | prints out the first 10 lines of a file               |
| tail    | prints out the last 10 lines of a file                |
| echo    | print to stdout                                       |
| file    | determine file type                                   |
| grep    | matches patterns                                      |
| wc      | "word count" returns number of lines, words, or bytes |

Since we'll need a file to use as an example, run `wget https://www.w3.org/TR/PNG/iso_8859-1.txt` to download an sample text file. _W3_ is the site for the _World Wide Web Consortium_ who helps define standards for the web; however, that's not important right now. The file is just to serve as an example text file. `wget` is another utility we'll get into later that downloads items from the web. 

We'll also introduce __piping__ and __redirection__ here because, more often than not, it'll be used to explore files. We'll start with the basic pipe (i.e. `|`) and redirect (i.e. `>` or `<` ). Pipes pass output to another program, versus redirects which pass output to a file or stream. For instance, let's take `cat` and `grep`. If you run `cat *.txt | grep file`, it will spit out all the content of all the files that end with `.txt`, search through them, and return the lines that have the word "file" in them. My `file1.txt` has a single line that reads `this is from inside fi1e1.txt`. If you run `file1.txt > redirect.txt`, you'll see that it redirects everything from `cat` intro the new file. If you do it with `file2.txt`, you'll see it will overwrite the target. If you double the redirect (i.e. >>), it appends to the file instead of overwriting it. It's also worth noting you can chain commands as well with `&&`. If you run `cat file1.txt && cat file2.txt`, the contents of both files will be printed to the screen.

`head` and `tail` by default print out 10 lines. You can specify a number by using the option `-n`(i.e. `head -n file1.txt`)

You can use `file` to determine the file type. `file iso_8859-1.txt` should tell you its a text file made of ASCII text. Also, in the last section we used `touch` to create an empty file. Just to reiterate this is empty, run `file new_file.txt`, and it will tell you it's empty. 

<script id="asciicast-366159" src="https://asciinema.org/a/366159.js" async></script>

---

## Other useful commands and tools

So far, we've touched on the basics and piping/redirecting. I'll close up this post with a few other entries that should be in your repertoire, including some that aren't commands.

| Command             | Description                                                  |
| ------------------- | ------------------------------------------------------------ |
| sudo                | "superuser do"; elevates your privileges                     |
| less                | prints out contents until your screen is full, then it pauses. Continue by pressing _Enter_. Exit by pressing _q_. |
| history             | everything you've typed in the command line; notice these are indexed on the left |
| tar                 | archives files into a single file for manipulation           |
| chmod               | change the mode of a file                                    |
| chown               | change the ownership of a file                               |
| ping                | sends a signal to a website and waits for a response         |
| wget                | downloads files from the internet                            |
| curl                | exchange requests or responses from servers                  |
| df                  | disk space free                                              |
| clear               | clears your terminal                                         |
| !!                  | returns your last command                                    |
| ![# from _history_] | execute that command                                         |
| exit                | exits out of the terminal; ends your session                 |

| Keystroke    | Desctiption                                                  |
| ------------ | ------------------------------------------------------------ |
| _Ctrl_+_L_ | clears your terminal; same as typing `clear`                 |
| _Ctrl_+_C_ | cancels out of your current process                          |
| _Ctrl_+_Z_ | pauses out of your current process; use `fg [command] to resume` |
| _Ctrl_+_D_ | exits out of the terminal; ends your session; same as typing `exit` in the terminal |
| _Ctrl_+_S_ | suspends any output to your terminal; resume with _Ctrl_ + _Q_. |
| _Ctrl_+_U_ | cut text from the beginning to before the cursor             |
| _Ctrl_+_Y_ | paste                                                        |
| _Ctrl_+_A_ | move cursor to the beginning of the line                     |
| _Ctrl_+_E_ | move cursor to the end of the line                           |
| _Tab_        | tab completion; double-tap to display all the possibilities  |

`sudo` basically escalates your privileges to that of an administrator. If you're on public access, you are probably blocked from using it. Beginning users will probably use it for basic tasks like updates and editing your file system outside of your home directory. Sorry, no examples for this one. 

`tar` makes dealing with multiple files easier by archiving it into a single file, a _tarball_, in the event you need to maybe upload it to a server. You can also compress it simultaneously. `tar -czvf` is what is usually used when compressing a file. `-c` tells it to create an archive, `-z` compresses the archive, `-v` stands for _verbose_ and displays the progress to the terminal, and `-f` allows you to specify the filename. `gzip` and `gunzip` is what's also used to zip and unzip `.gz` files. Archived and compressed files usually have the suffix `.tar.gz`.

`chmod` changes the mode of your file. Modes designate read, write, and execute access. Run `ls -l` and inspect the modes for all the files. For example, to add read and write privileges to your file for your group, run `chmod g=rw filename`.  Run `chmod g=-rw,u=-r filename` to remove those rights and the read rights for others. If you start running any scripts, your going to start to want adding the `+x` mode.

`wget` is usually only used to download files through `HTTP/HTTPS` or `HTP` requests. `wget` is only on the command line. `curl` is based on the library `libcurl`, so it can be found on a variety of platforms. `curl` can still be used to download files, but it can also support a lot more protocols. Default is to also spit everything out into the terminal. if we take that same W3 address for the text file as above and use `curl`, its contents will be spilled onto the screen, which can be ideally piped into something else. This can easily be turned into some kind of web scraper if need be.

 `df -h` will give you the free space on your drives. The one of most relevance is probably `/home`.

`!!` returns the last command. This is helpful when you forget you need `sudo` permissions, so just enter a quick `sudo !!` to fix it. You can also use `!` to run one of your previous commands. View `history` and take note of that number on the left of whatever line you want to enter again. if you run `!#` it should execute it again.

<script id="asciicast-366160" src="https://asciinema.org/a/366160.js" async></script>

This of course isn't everything; however, these are the ones I probably use the most. 
