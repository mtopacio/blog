---
title: "Umami - Website Analytics"
date: 2020-09-20T15:30:57-07:00
author: Mark Topacio
categories: ["Python","virtual","environment","foss"]
draft: true
---

One of the things you can integrate wit *Hugo* is website analytics. There are paid-for-services like [Google Analytics]() or [Fathom]().

And then there's [Umami](https://umami.is/), which started off as a project by Mike Cao to provide all these features in an open-platform. 

To install, the minimum requirements are a server with MySQL or PostgreSQL, and a server with Node.js 10.13 or newer. This rundown is basically a copy-paste of his instructions.  

```bash
$ git clone https://github.com/mikecao/umami.git
$ cd umami
$ npm install
```

Create a database on your server. In my case, I'm using PostgreSQL.
```sql
username-# CREATE DATABASE database_name;
````

Run the sql script that comes with the installation.
```bash
$ psql -U username -d databasename -f sql/schema.postgresql.sql
```

I created a seperate account for dealing with Umami.
```
umami-# CREATE USER username WITH PASSWORD 'password';
umami-# GRANT ALL PRIVILEGES ON DATBAASE umami TO analytis;
```
