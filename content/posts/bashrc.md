---
title: "bashrc"
date: 2020-09-20T10:08:27-07:00
author: Mark Topacio
categories: ["bash","Linux"]
draft: false 
---

Since I threw out an example of my _Vim_ customizations, I thought I'd take a step back and talk about `.bashrc`. This is basically just a shell script that gets executed everytime you start a terminal session. It's not really necessary for your system, but customizing this file goes miles in making your workflow more efficient. 

---
### Environmental Variables

You can use bash to customize your prompt, which is that small string you get before your cursor. Mine is rather plain and not that far from the default `username@localhost: $`. I've customized mine with specific colors followed by the current directory.

```bash
PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\W\$: '
```

---
### Aliases

The majority of people use `.bashrc` for creating aliases in their environment. Aliases provide an alternative trigger for another command. For instance, `alias ls='ls --color=auto` is in most of everyone's `.bashrc` file by default. `ls` by default just lists everything in your directory with no distinction between files types. Replacing `ls` with `ls --color="auto"` lets you colorize the output based on types. Rather than typing `ls --color=auto` everytime, all you have to do now is type `ls`.

For instance, to use _Hugo_ with _Gitlab_, I have to push my files/changes using _git_. Rather than type out whole phrase `git add [filename] or `git push origin master`, I have aliases in the form of `alias ga="git add"` and `alias gpush="git push origin master"`. Within my `.bashrc`, it looks like:
```bash
alias ga="git add"
alias gpush="git push origin master"
alias gpull="git push origin master"
````

I usually have a small number of aliases dedicated to ssh. Note your can also set variables as you would in a bash script. These lines are also paraphrased as to not get too personal.
```bash
keys="/home/mtopacio/some/ssh/key/folder"
alias sshHome="ssh -i ${keys}/home_key -p 9999 me@home.com"
alias sshMach1="ssh -i ${keys}/machine1_key -p 9999 me@machine1.com"
```

Another small block of aliases are dedicated to all my virtual environments, which I explain more [here](https://mtopacio.gitlab.io/blog/posts/virtualenv/). I'll usually always set up an alias to access my more permenant ones directly. I also have a line for some of those one-timers usually used when I'm in my project directory"
```bash
v-quant="source /home/mtopacio/quant/venv/bin/activate"
v-temp="source venv/bin/activate" # change into project directory first
````

You should also set up a quick little alias for directories you'd usually frequent or other _ad hoc_ situations. 
```bash
alias blog="cd /home/mtopacio/blog/"
alias ip_address="python /home/mtopacio/adhoc/ip_address.py"
```

---
### Functions

Since `.bashrc` is just a big _bash_ script, you can also declare functions to be a permanent addition to your environment. If you wanted make consecutive commands or ask for user input, this would be the route to take.

Going back to that _git_ example up top, I have a function for when I want to commit in order to capture the description.
```bash
function gc {
    read -p "Enter git comment: " comment
    git commit -m "${comment}"
}
```

When using Arch, programs outside the standard package manager (_pacman_) can usually be found in the [AUR](https://aur.archlinux.org/). I have a small function to grab those files for an install with the apporpriate program name. 
```bash
function aur (
    AUR_DIR="/home/mtopacio/AUR"
    CUR_DIR=$(pwd)
    if [ $CUR_DIR != $AUR_DIR ]; then
        cd $AUR_DIR
    fi
    git clone https://aur.archlinux.org/${1}.git
    makepkg -i
)
```

---
###`.bash_profile` and `.bash_logout`

Your `.bashrc` file would normally exist under your home directory along with other `.bash_*` files unless your system admin has set it up differently. There are `.bash_profile`, `.bash_logout`. and maybe even `.bash_aliases`. These files have a similar function with the difference of when they're executed.

If there is a `.bash_aliases`, there will also probably be a line in your `.bashrc` referencing it, meaning these two will be executed at the same time. It's just another file for housing aliases specifically. Any changes to your environment should be housed in `.bash_profile`, which is executed anytime you log in, and only then. It doesn't get executed everytime you create a new interactive terminal session. Due to this, it's recommended you keep any adjustments to your `$PATH` variable here. If you include it in your `.bashrc`, and you keep creating sessions, `$PATH` might become unneccesarily long.

`.bash_logout` is the last of the `.bash` files, and this one is ran everytime your logout as you may have guessed. You may want to include any shopkeeping items here to delete any temporary files or backup any work.

