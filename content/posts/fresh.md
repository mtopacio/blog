---
title: "LVM"
date: 2020-09-20T10:08:27-07:00
author: Mark Topacio
categories: ["lvm","Linux"]
draft: true 
---


Fresh Install

sudo pacman -S xorg plasma kde-applications htop libreoffice gcc-fortran r 
# git and openssh is usually installed during the installation process when chrooted into root


sudo systemctl enable sddm.service


# AUR packages
firefox-nightly 
nerd-fonts-inconsolata
nordvpn-bin
rstudio-desktop-bin

sudo systemctl enable nordvpnd.service
sudo systemctl start nordvpnd.service
