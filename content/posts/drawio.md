---

title: "diagram.net"
date: 2020-10-14T10:08:27-07:00
author: Mark Topacio
categories: ["diagram.net","FOSS","self-hosted"]
draft: false
---

It's taking me a while to come up with any progress on my project management app. I've been splitting up my time between scripting something for work and another data visualization project for Halloween. Since I don't have anything to update as of yet, I thought I'd throw out a few reviews.

The first of which is for [diagrams.net](https://www.diagrams.net/), or formerly known as _draw.io_. I definitely cannot recommend this application enough. They have an [online version](https://app.diagrams.net/) you can start using right away, as well as a desktop client. It's also integrated by a few different softwares, like [_NextCloud_](https://nextcloud.com/), which is the version I currently use. Their [github](https://github.com/jgraph/drawio) also mentions you can use _Github pages_ to host a version if you fork the project over and publish the master branch. The only caveat is there is no supported integrations in this version.

The use-cases I have come across are for flowcharts and mock-up diagrams. It's fairly intuitive and you can jump into it right away. The only other software I've used to make flowcharts is _Powerpoint_; however, the ability to import different types of shapes to be used as buttons and pathways has made this my go-to, especially since it's self-hosted.

{{< figure src="/blog/images/drawio_1.png" caption="<sup>They have a fully functional [version](https://app.diagrams.net/) online.</sup>">}}

The layout is workspace oriented with yabs on the bottom to swtich between multiple projects. On the right are going to be the settings you can play with. It's here you'll usually change the style of your elements, the text if any, and its arrangement in your layout. On the left are all the elements you can include. Double-clicking will place one in your workspace. If these aren't enough you can also import more by clicking **+ More Shapes**.

{{< figure src="/blog/images/drawio_2.png" caption="<sup>Clicking **+ More Shapes** will bring up another window with differemt sets of shapes you can add to your collection.</sup>">}}

You can find shapes for web page mock ups, entity-relation diagrams, network maps, floor plans, electrical diagrams and more. You can also export your project as different formats, including HTML files you can embed in a _Hugo_  blog. For the project management app, it was too hard to visualize the entire project and where I wanted to head, so I used it to create a little mock up of what I think my app should look like from the moment you land on it. 

<iframe src="/blog/drawio/project_management.html" width=100% height=500></iframe>

I've also used it to create a network map (IPs removed) of my homelab. 

<iframe src="/blog/drawio/network.html" width=100% height=500></iframe>

