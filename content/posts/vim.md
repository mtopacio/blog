---
title: "Vim"
date: 2020-09-20T10:08:27-07:00
author: Mark Topacio
categories: ["vim","Linux"]
draft: false
---

_Vim_ is the editor I always come back to. I've tried other IDE's; however, I tend to always come back to _Vim_ because of it's super useful from the command line and it's easy to implement my configs if I need to work on a virtual server. This is just a quick rundown of some of the shortcuts I usually use and what I usually end up changing after a fresh install. _Vim_ is usually in most repositories, so installation is painless.

```bash
$ sudo pacman -S vim
# Debian/Ubuntu
$ sudo apt-get install vim 
```

Most of your _Vim_ customizations will go in the file `~/.vimrc`. It works similarly to `.bashrc`; however, generation of this file is not automatic. You may have to create it after a fresh install.

---

## Shortcuts

For all of these shortcuts, `ESC` gets out of the current mode and leaves you in _command_ mode, which is where you can press or enter any of the following keys and get an action. VIM has its own hot keys for movements, so there's no need to fret if you want to use a 60% keeb without any dedicated arrows. A full list of _Vim_ keyboard shortcuts can be found from Damien's [cheatsheet](https://www.maketecheasier.com/cheatsheet/vim-keyboard-shortcuts/).

| Key(s) | Cursor Movement       |
| ------ | --------------------- |
| 0      | beginning of the line |
| $      | end of the line       |
| w      | one word forward      |
| 5w     | five words forward    |
| b      | one word back         |
| 5b     | five words back       |
| G      | end of the file       |
| gg     | beginning of the file |
| [[     | next section          |
| ]]     | previous section      |



Those were just movements. The following table lists some of the hot keys most frequented as well as some commands (i.e. these are distinguished by colon (:) as the first character):

| Key(s)                     | Description                                        |
| -------------------------- | -------------------------------------------------- |
| i                          | insert text at cursor                              |
| yy                         | copy the current line into memory                  |
| p                          | paste whatever is in memory after the current line |
| /text                      | search for "text" in file                          |
| n                          | move to the next instance of the result            |
| N                          | move to the previous instance                      |
| :%s/original/replacement   | replace the first occurrence of "original" text    |
| :%s/original/replacement/g | replace all instances of the "original" text       |
| :q                         | quit _Vim_                                         |
| :q!                        | quit _Vim_ without saving                          |
| :w                         | save file                                          |
| :wq                        | save file then quit                                |
| :w filename                | save file to new filename                          |

---

If these key bindings are a little intimidating, [_micro_](https://micro-editor.github.io/) is an editor that looks promising. It basically looks like _Vim_ and offers the same functionality; however, the key bindings are a little more traditional in that they mimic those you would find from a GUI-based editor. 

## Plugins

IDEs are usually attractive because of all the functionality their features provide; however, these have usually come at a cost of being resource heavy or limited to your local environment. I used to use [_Atom_](https://atom.io/); however, that was too taxing on my ebook. If that isn't an issue for you, then give it a try because it has support for multiple languages along with other helpful plugins that make it incredibly useful. I'm not sure about its ranking anymore, but lately, I've been hearing a lot about _VSCode_ instead, especially after they released a version for Linux. If you're looking for a GUI, I've also really enjoyed [_Geany_](https://www.geany.org/) and [_Notepad++_](https://notepad-plus-plus.org/). 

I'd prefer to stick with just one environment, and because of that, a low resource footprint was at the top of the priorities.  Second on that list is to be able to use it through my terminal due to _ssh_. Both of these have me coming back to _Vim_ every time I try something else. 

A lot of functionality found in GUI-based IDEs can easily be supplemented by plugins in _Vim_. To install them, I use [_vim-plug_](https://github.com/junegunn/vim-plug) which is a minimal plugin manager for _Vim_. Installation is relatively easy. 

```bash
$ curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

This will download their _Vim_ script in your `autoload` folder. You're then going to create a section in your `~/.vimrc` file to list all your plugins. 

```bash
# specify a directory for all your plugins to be downloaded
call plug#begin('~/.vim/plugged')

# start listing the ones you want to install
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

# initialize plugin sysem
call plug#end

# theme for vim-airline
let g:airline_theme='wombat'
```

Exit the file, reload _Vim_, and run `:PlugInstall`. Another window should pop up within _Vim_ showing your downloads/installations. To update, run a similar `:PlugUpdate`.

---

## Other Small Tweaks

There's a few other lines I have included in my `.vimrc` file besides plugins to help with my workflow.

`command! -nargs=0 Sw w !sudo tee % > /dev/null` allows me to save files under `sudo` when I've opened it as a normal user. Normally, if you try and save over an edited file you need elevated privileges for, you'll have to exit without saving. This line allows you to enter `sudo` mode without exiting and using `sudo vim [filename]`.

`set number` adds line numbers along the left border.

`set ttyfast` speeds up scrolling.

`syntax on` allows syntax highlighting.

If you've ever used _Python_, you know it's a stickler for spacing. Best practice is to set everything as spaces, and use 4 spaces to indent. These lines help with maintaining that.

```bash
filetype plugin indent on
set shiftwidth=4
set softtabstop=4
set autoindent
set expandtab
```

`autoindent` indents your lines for you. For instance, if you're entering multiple lines of a for-loop, when you hit enter, it'll start you off aligned to the rest of your block. 

`expandtab` inserts four spaces when you press the `Tab` key.

`set hlsearch` highlights your searches.

`set wrap` wraps long lines when they extend past the window.

That's it. I'll usually save a copy of this in case I come across a new machine. I'll `scp` it over and run `vim` with `:PlugInstall`. In seconds, I'll have the same working environment I am used to. 

The following is my actual `.vimrc` file. It lists a couple other plugins that I use along with the tweaks I just mentioned. 

```vim
" REMINDERS #####################################
"
"   :PlugInstall to install plugins
"   :PlugUpdate to update plugins
"   :PlugClean + delete plugin from .vimrc to uninstall
"
"   zf    create a fold
"   zo    open a fold
"   zc    close a fold
"   zd    delete a fold
"
" GENERICS ######################################

command! -nargs=0 Sw w !sudo tee % > /dev/null  " save as sudo :Sw
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview

set number                  " add line numbers
set ttyfast                 " speed up scrolling
set shiftwidth=4
set softtabstop=4
set autoindent
set expandtab
set wrap                   
set hlsearch                " highlight search
set foldenable              " enalbe folds on start up
set foldmethod=indent       " tabs define folds
set foldlevel=2             " lines with 2+ tabs will be folded
syntax on                   " turn syntax highlighting on

" PLUGINS ########################################

filetype plugin indent on   "for plugins to load correctly
call plug#begin('~/.vim/plugged')

Plug 'vim-airline/vim-airline'              " status bar
Plug 'vim-airline/vim-airline-themes'       " more themes for airline
Plug 'scrooloose/nerdtree'                  " file explorer
Plug 'alvan/vim-closetag'                   " autocloses html tags
Plug 'townk/vim-autoclose'                  " autcloses quotes, parenthese
Plug 'scrooloose/syntastic'                 " syntax checker
Plug 'nvie/vim-flake8'                      " python highlighting
Plug 'pearofducks/ansible-vim'              " Ansible highlighting
Plug 'yggdroot/indentline'                  " indentation lines "

Plug 'junegunn/goyo.vim'                    " turns vim into a reader
" usage:
" :Goyo - toggle Goyo
" :Goyo [dimension] - turn on or resize
" :Goyo! - turn off Goyo

call plug#end()

" PLUGIN CUSTOMIZATIONS ########################     

let g:airline_theme = 'wombat'
let g:airline#extensions#ale#enabled = 1
let g:ale_sign_column_always = 1
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'

let g:closetag_filenames = '*.html, *.xhtml, *.phtml, *.vue'
let g:closetag_shortcut = '>'
let g:closetag_close_shortcut = '<leader>>'

let python_highlight_all = 1

let g:indentLine_defaultGroup = 'SpecialKey'
let g:indentLine_char = '|'

" KEY MAPPINGS #################################

map <C-r> :!clear;python3 %<CR>
map <C-n> :NERDTreeToggle<CR>
map <C-g> :Goyo<CR>
```
