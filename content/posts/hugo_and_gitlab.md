---
title: "Hugo and Gitlab"
date: 2020-08-29T09:35:16-07:00
author: Mark Topacio
categories: ["Hugo","Gitlab","FOSS"]
draft: false
---

This post is describing how I set up a blog using _Hugo_ and _Gitlab_'s static site support. I wanted a place to post some of my own code in addition to my public git repositories. Somewhere I can also expand on my train-of-thought versus just posting straight code with a comment or two peppered in between. I used to host a static site on a RPi3 locally; however, I recently decided I wanted to use off-site storage as a form of redundancy with extra reliability. I still host a git server locally using [_Gitea_](https://gitea.io/en-us/), but the blog portion has been removed. If interested, _Gitlab_ also offers a way to host a repository locally; however, I went with a lightweight alternative to save on resources. 

_Gitlab_ is a web-based DevOps tool built around _git_ that also provides issue-tracking and continuous integration/continuous deployment pipelines. There's a free tier which includes all stages of the DevOps lifecycle and 2000 CI/CD minutes. It also gives you the ability to bring your own CI runners and production environments. Likewise, you can also locally host a _Gitlab_ server under a similar hierarchy. Free gets you the base open-source core. Anything more will cost you. _Git_ is a free and open-source distributed version control system used to coordinate work between programmers. It was designed by Linus Torvalds to help with another of his projects, the _Linux_ kernel.

_Hugo_ is a static site generator, meaning everything has fixed content versus dynamic (i.e. can change depending on user input). There are alternatives, like [Jekyll](https://jekyllrb.com), that can serve up static sites; however, I've been set on _Hugo_ for awhile now due to its simplicity and speed. I will say that more people seem to lean towards _Jekyll_, which is important to note when considering support and features. _Hugo_ builds the .html files with a theme you select, including css and javascript, and it will wither host it from memory using the built-in server or dump the .html files into a directory you can link to your own web sever. 



## Install Hugo

First thing to do is install _Hugo_. I'm running an Arch system where the default package manager is _pacman_. These instructions were taken from _Hugo_'s instructions on [how to install](https://gohugo.io/getting-started/installing/) and [how to get started](https://gohugo.io/getting-started/quick-start/).

```bash
$ sudo pacman -Syu hugo
```

That's it. Hugo should now be installed. Next, you're going to want and get a site going, which you'll then push to _Gitlab_. These instructions are taken from another page from _Hugo_ that specifically addresses [using _Gitlab_ as a host](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/). Create a project using one of _Hugo_'s commands. After that your going to find a [theme](https://themes.gohugo.io/) and create a submodule, edit some configurations, commit, and push. For this example and this blog, I'm using the theme [_bento_](https://themes.gohugo.io/bento/).  In the following example, replace `blog` with the name of your project. _Hugo_ will create a directory with that name with the necessary parts. 

```bash
$ hugo new site blog
$ cd blog
$ git init
$ git submodule add https://github.com/leonardofaria/bento.git themes/bento
$ echo 'theme="bento"' >> config.toml
$ hugo server -D
```

The last command `$ hugo server -D` will use _Hugo_'s internal web server to host your page locally. I should also note even _Hugo_ does not recommend you use this server for production, only for development. You can normally access your page at [https://localhost:1313](https://localhost:1313).

It was at this point, however, that I ran into my first set of problems. 

{{< figure src="/blog/images/hugo_and_gitlab_1.png" caption="<sup>It kept giving me errors about not finding any layouts for the site I was tying to build. From what I read, this came from an issue that prevented the creation of your CSS files. </sup>">}}

This was resolved by reinstalling _nodejs_, _npm_, and the relevant dependencies.

```bash
$ sudo pacman -S nodejs-lts-erbium
$ sudo pacman -S npm
$ sudo npm install postcss-import tailwindcss postcss-reporter @tailwind.ui
```

When I ran `$ hugo server -D` again, it followed through; however, another issue popped up where my images weren't being displayed. The solution didn't really warrant the amount of time I spent on it. I don't really know how _Hugo_ handles absolute and relative paths, but the link in my images were wrong. I compared the image for the home page, which was working, and that of my post. The home page had the path [https://localhost:1313/blog/images/san_diego_city_view.jpg](); whereas, the image in my post had the path [https://localhost:1313/images/hugo_and_gitlab_1.png](). For image pathways, adding the [/blog]() fixed everything. If for some reason, you're having trouble getting things to show up, pay attention to the paths. If there are no errors to go off of, that will help you figure things out.



## Gitlab

Hugo is up and running. Now, the goal is push this project to _Gitlab_ and have them host your static site.

``` bash
$ vim .gitlab-ci.yml
```

`vim` is the text editor I prefer; however, use whatever editor you prefer to edit [`.gitlab-ci.yml`](https://gitlab.com/mtopacio/blog/-/blob/78d3bb1f49f0027632e211fef2196cd7a1a3b884/.gitlab-ci.yml).This is a small _docker_ script that tells _Gitlab_ on how to process your page in a pod. 

```
# .gitlab-ci.yml

image: monachus/hugo

variables:
	GIT_SUBMODULE_STRATEGY: recursive
	
pages:
	script:
	- hugo
	artifacts:
		paths:
		- public
	only:
	- master
```

After that, take a second to edit your `config.toml` file as well. The following is the default `config.toml` file that _Hugo_ creates in your main site directory. The only difference is the line you added above specifying the theme. More parameters can be added if needed. Refer to your theme's documentation or website. Their _live version_ usually has posts summarizing how to use their theme. Make sure you change the `baseURL` to point to your actual blog. _Gitlab_ places your repo under the url [http://gitlab.com/username/blog](); however, your page will be located under the link [https://username.gitlab.io/blog]().

```
# config.toml

baseURL = "http://mtopacio.gitlab.io/blog/"  # change your site's url
languageCode = "en-us"
title = "Excercies in Programming and Analytics"
theme = "bento"
```

Last file you're going to create is a `.gitignore` file, which is self-explanatory. It tells which files _git_ will ignore when adding to a commit. 

```bash
$ vim .gitignore
```

```
# .gitignore

/public
/node_modules
package-lock.json
```

`/public` is the directory _Hugo_ creates to :house the .html files for your site when you use `$ hugo -D`. You don't need this since _Gitlab_ will produce its own .html files through the _docker_ script you wrote up above. `/node_modules` and `package-lock.json` were both created `npm` when I reinstalled the dependencies to deal with the issue above. If you didn't have any problems, you could omit those lines.

Then that's it. Double check you're in the top-level directory of your project, make a commit, add the origin, and push your project to _Gitlab_. Notice this is the url of your project, which ends in a .git extension, and not your website.

```bash
$ git add .
$ git commit -m 'Initial commit'
$ git remote add origin https://gitlab.com/mtopacio/blog.git
$ git push -u origin master
```
Give it a minute or two. _Gitlab_ will open a _docker_ pod to process your script. 

{{< figure src="/blog/images/hugo_and_gitlab_2.png" caption="<sup>You can check the status if you go to _CI/CD_ > _Pipelines_ under your project.</sup>">}}

Once it reads 'passed', you should now have a _Gitlab_ hosted static site. 

#### Edit

Another edit for those that want to use _Bokeh_ with _Hugo_. The instructions I followed are found on their user guide [Embedding _Bokeh_ Content](https://docs.bokeh.org/en/latest/docs/user_guide/embed.html). Make sure the following elements are included somewhere in your script alongside all the other _Bokeh_ code:
```python
from bokeh.resources import CDN
from bokeh.embed import file_html

html = file_html(plot, CDN, "some title")
```
You can also use the `html` file _Bokeh_ spits out when using `output_file` and `show`/`save`. Both default into using CDN for the BokehJS. `file_html` is meant to be used with custom templates.

Just stick the source in an iframe similar to the image up top:
```html
<iframe src="/blog/bots_and_bokeh/histogram.html" width=100% height=500></iframe>
```

If everything works correctly, you should see your plot.

<iframe src="/blog/bots_and_bokeh/histogram.html" width=100% height=600></iframe>

