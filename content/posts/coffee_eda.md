---
title: "EDA: Coffee"
date: 2020-12-20T10:08:27-07:00
author: Mark Topacio
categories: ["coffee","python","pandas"]
draft: true
---

# Exploratory Data Analysis: Coffee

This post is going to go over a small exploratory data analysis (EDA) using _Python_  from start to finish. This is going to include scraping and aggregating the data from the _Coffee Quality Institute_, then using _pandas_ to preprocess and run some basic statistics. 

### Coffee Quality Institute

The [Coffee Quality Institute](https://www.coffeeinstitute.org/)(CQI) is a non-profit dedicated to educating people on the standards and practices behind their coffee. They host a database of different types of coffee that have been submitted for grading which we are going to peruse for some insight. 

### Selenium 

_Selenium WebDriver_ allows you to drive a browser as a user normally would, but through a program. In a lot of cases, _Selenium_ is used to test functionality of website by going through everything and making sure it works. In our case, we are going to use the _Python_ bindings to get past the CQI database login page. 

```bash
# OPTIONAL: create a virtual environment for our little project
$ virtualenv venv
$ source ./venv/bin/activate
(venv) $ pip install selenium
```

We also need to download the actual driver for the browser _Selenium_ is going to interact with. There are drivers for *Chrome*, Edge, *Firefox*, and *Safari*. I'll be using the one for _Firefox_ linked [here](https://github.com/mozilla/geckodriver/releases). Extract it and make sure its in your `PATH` variable. You can either append it or place it in `/usr/local/bin`, which is what I ended up doing.

```bash
(venv) $ cd ~/Downloads
(venv) $ tar xvf geckodriver-v0.29.0-linux64.tar.gz
(venv) $ sudo mv geckodriver /usr/local/bin
# then cd back into your porject directory
```

Double check everything is working. Write up a small test script to verify.

```python
# scraper.py
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Firefox()
driver.get("https://www.python.org")
```

Try running that via `python scraper.py` and see if it runs. When I did it, it threw me an error because it couldn't find the browser binary. If you get a "Expected browser binary location, but unable to find binary in default location..." error, you're going to gave to define the path where it can be found. Change `scraper.py` to the following:

```python
# scraper.py
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.keys import Keys

# this will be different for you. make sure it points to your binary
# in my case, I run Firefox Nightly
binary = FirefoxBinary('/usr/bin/firefox-nightly')
driver = webdriver.Firefox(firefox_binary = binary)
driver.get("https://www.python.org") 
```

For me, a browser popped up with _python.org_. But I still don't want that. This is going to be run silently in the background. Plus, we need the actual website and also update the driver line.

```python
# add the following import statement
from selenium.webdriver.firefox.options import Options

database_url = "https://database.coffeeinstitute.org/login"

options = Options()
options.headless = True
binary = FirefoxBinary('/usr/bin/firefox-nightly')
driver = webdriver.Firefox(options = options, firefox_binary = binary)
```

Navigate to their database's landing [page](https://database.coffeeinstitute.org/) and create an account.

Now starts the grunt work. We need to find each element we're going to interact with. For the login page, it's easy enough. There's only the login widget. We can use `driver.find_element_by_name('username')`. Same goes for the password. The submit button is going to be a little different. We don't have a name to find it by, so we'll use the _xpath_.

```python
login_name = driver.find_element_by_name('username')
login_name.send_keys(db_user)
login_pass = driver.find_element_by_name('password')
login_pass.send_keys(db_pass)

driver.find_element_by_xpath("//input[@type='submit']").click()
```

