---
title: "Google's Gmail API and Python"
author: Mark Topacio
categories: ["Google", "Python", "Gmail", "API"]
date: 2021-02-23T09:13:17-07:00
draft: true

---

This will focus on the the data aggregation part of my ETF tracker. I've to the [Python Quickstart](some site) portion of Google's documentation for their Gmail API. I clicked _Enable the Gmail API_, which gave me another box where I could select my _etf-tracker_ project. They type of app I am going to create I specified as _Desktop_. A _client ID_ and a _Client Secret_ key was populated. Download the `credentials.json` file they provide and store the _Client Secret_ key somewhere. Copy-paste the code they use for `quickstart.py` and run it once everything is in the same directory.

This is whole authentication process is going to require you to log in through your browser; however, we're going to use Selenium to power through this. The script should give you the url that populates your browser. Copy-paste that into another script where we'll interact with it via Selenium.