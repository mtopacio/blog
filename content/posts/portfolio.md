---
title: "Portfolio"
author: Mark Topacio
categories: ["Flask","PostgreSQL","Gunicorn","WSGI","systemd","Firebase","Vue","npm"]
date: 2020-10-16T09:13:17-07:00
draft: true 
---

I want to aggregate all my visualizations into more of an interactive demo; however, I want to explore outside hosting services in lieu of my home server to reduce the amount of traffic going in and out of my network. Eventually, I'm looking to offload my HTTP/HTTPS requests and close those ports entirely. The less open, the better. Since there's not a lot of traffic to begin with, I can probably get away with a small app on a free tier. For this app, I decided to try out Google's [_Firebase_](https://firebase.google.com/).

I also recently started learning [_Vue.js_](https://vuejs.org/) which is a JavaScript framework that helps create reactive web pages. _Vue_ seems to have a much more approachable ecosystem when compared with other popular frameworks, like [_Angular_](https://angular.io/) or [_React_](https://reactjs.org/). _Vue_ is also an open-source project purely backed by the community; whereas, _Angular_ has the backing of Google and _React_ Facebook. I'm not that familiar with front-end development, so I thought this would be a good opportunity. 

Elaborating more on this whole project's structure, I want to host my front-end app on _Firebase_, which I will have written with _Vue_. From that front-end, I want to pull data from my remote _PostgreSQL_ database. It will be unauthenticated read-access _only_ through a RESTful API via _Flask_. All the data aggregation and writing to the database will be handled on my _Google_ compute engine via _Python_ scripts. 

## _npm_ and _Vue.js_

To get started, _npm_ needs to be installed on your machine, which is the package manager for _Node.js_, an asynchronous event-driven Javascript runtime. For whatever reason, _npm_ was installing to a folder that needed escalated privileges, so I added a prefix to install to a local folder. Once that was installed, go ahead and install _Vue_ and create a project.

```bash
$ sudo pacman -S npm	# nodejs is one of its dependencies
$ mkdir ~/.npm-global
$ npm config set prefix '~/.npm-global'
$ npm install --global vue @vue/cli
$ vue create portfolio 	# replace portfolio your own project name

```

I'm going to keep this project separate from my blog. If you want to look through the source code, visit its [repository](). `$ vue create portfolio` should create a directory in your current folder with the same name.  If you `cd` into there, and run `$ npm run serve`, _npm_ will deploy a development server you can use to view your project though [http://localhost:8080/](http://localhost:8080/). By default, a sample page is included with links to more _Vue_ resources. 

{{< figure src="/blog/images/portfolio_1.png" caption="<sup>If everything worked right up to this point, you should be looking at _Vue_'s default webpage.</sup>">}}

## Firebase

This might be backwards, but I want to try deploying the app before building it. Go ahead and visit _Firebase_ and add a project. Google will then ask you a number of questions regarding analytics and your application's privacy. Once you make it past all this, it'll bring you to your project's dashboard. Go to _Develop_ > _Hosting_ on the left-hand side. This will give you a small rundown on how to initialize your project as a _Firebase_ app and deploy it with _npm_.

{{< figure src="/blog/images/portfolio_2.png" caption="<sup>Click on __Hosting__ for instructions on how to deploy to _Firebase_.</sup>">}} 

```bash
$ npm install --global firebase-tools

# sign in to Google and initiate your project
$ firebase login
$ firebase init
```



`$ firebase login` will launch a web page where you can login with your Google credentials. After you agree to giving Google more permissions, you can enter `$ firebase init`. This will launch the initialization process for your project.  When asked about which features, the only one I selected was _Hosting: Configure and deplot Firebase Hosting sites_. Press space to select and enter to confirm. It will then ask you to select an option regarding which project to use. Select _Use an existing project_ with space and confirm with enter. It'll pull up the names if your existing projects. In the image above this, you can see I've named my project _portfolio-topacio_. This is the one I want to use. Use whatever project you've created. After that, it's going to ask you for your public directory, or the directory where your site is. In the case of _Vue_, when you run `$ npm run build` , it spits out the production site in a folder called `dist`. "dist" is what you want to enter. It then asked if I want to configure this as a single-page app, which I answered yes. Next is a question on if you want to set up automatic builds and deploys with _Github_. I answered no since the code will be hosted on my _Gitlab_ account along with the rest of my personal projects. 

Finally, `_Firebase_ initialization complete!`.

## _Compute Engine_ Instance for your Database

Your app is now initiated as _Firebase_ project. Now I want to deploy something to see if it works. Let's go back to that `build` command and create a production version of our app. Enter `$ npm run build` to create the `/dist` directory. If you look inside, you can find all the necessary parts of a web page including your `index.html` and the CSS and JS files. Now for deployment. Enter `$ firebase deploy --only hosting`. After a good minute, it'll print out a hosting URL you can use to access your app. Alternatively, a link is provided for your project console where you can also find your project's URL.

{{< figure src="/blog/images/portfolio_3.png" caption="<sup>Your project should now be listed on your dashboard.</sup>">}} 

Now I need someplace to store all my data. Since I'm already using Google, I'll use one of their services, especially one that's part of their [Free Tier](https://cloud.google.com/free). I also looked at _Heroku_, but their database use was limited to 10,000 rows or 5MB. I'm not really sure on my end-game for this portfolio, so I want to make the choice that makes the most sense. I'm going to assume you've also used up your free period and are subject to all the normal costs. The first options is  to stay in the _Firebase_ ecosystem and use _Firestore_, which gives you a 1GB NoSQL database. The second is to use  _Compute Engine_ to host an RDMS. The F1-micro instance falls under the free tier, which offers 0.2vCPU and 0.6GB of RAM. It also gives you 1GB of network egress from North America to region destinations. What's most attractive about this option is the 30GB HDD storage. Another choice is to use  Google's _Cloud Storage_, which provides 5GB-month Standard Storage, 5,000 class A operations (i.e. write to disk), and 50,000 class B (i.e. read from disk). It also gives you the same 1GB network egress from North America to region destinations. It doesn't really specify a limit on space, but I keep reading 5GB. 

I decided to go with the cheapest and the most space, the _Compute Engine_ instance. I might get hit with the hardware limitation, but that's only if people are actually looking through my projects. To start, you can go to your GCP landing page and open up the menu on the left-hand side. You're then going to go _Compute Engine_ > _VM Instances_. 

{{< figure src="/blog/images/portfolio_4.png" caption="<sup>Click __Compute Engine__ > __VM Instances__.</sup>">}} 

I don't have anything running, so the only thing I see is a window telling me to create, import or go through a quickstart. I chose _create_. One thing I didn't mention is I set my _Firebase_'s default location to __nam5 (us-central)__. When given the options, it was between this multi-regional location and some regions along the west coast. I chose this to provide more redundancy, This also plays a factor in the free tier, so pay attention to what location you select. In most cases, __us-central__ is supported by most offerings. In alignment with my apps location, I also selected __us-central1 (Iowa)__ for my instance's region. In addition to entering a name for my app and setting the location, I also entered __N1__ for the _Series_, __f1-micro (1 vCPU, 614 MB memory)__ for the _Machine type_, __Debian GNU/LINUX 10 (buster)__ for the boot disk, and a custom hostname. I left the firewall as default. We'll set that up later.

{{< figure src="/blog/images/portfolio_4.png" caption="<sup>Example set up of my instance.</sup>">}} 

After selecting all my settings, the monthly estimate came out to $5.08; however, also notice in the text below, the first 744 hours are free. 24 hours x 31 days = 744 hours just to be explicit. That monthly fee should be waved unless for some reason requests skyrocket. 

Press __Create__. Once your instance has a green check, it's up and running.

The _Google Cloud Platform_ gives you the option to open up a console in the browser, or edit your instance with a CLI app; however, I'd rather do everything through _ssh_. Click your instance name, then edit up at the top. Scroll down and you should see a line that says "You have 0 SSH keys". Press the __Show and edit__ underneath to display a way enter your public _ssh_ key. 

### Connect to your Instance

Create an _ssh_ key to use for your instance. For my username, it was the same as my Google account. 

```bash
$ sudo pacman -Ss openssh
$ ssh-keygen -t ed25519 -a 100 -f ~/.ssh/key_name -C username
$ cat ~/.ssh/key_name.pub
# its gonna spit out a long string of characters with your username at the end
```

Take that string of characters, and copy/paste it into that box that appears after you clicked __Show and edit__. Save and _ssh_ into your instance. Your external IP address is also included in the VM instance details.

```bash
$ ssh -i ~/.ssh/key_name username@external-ip-address
```

Do the things you normally would on a fresh _Linux_ install, then install your RDMS. I'm going to be using _PostgreSQL_. _git_ is also manage some repos.

```bash
$ sudo apt update && sudo apt upgrade
$ sudo apt install postgresql git 
```

This might take a while. Remember, you only have 0.2 of vCPU and 0.6 RAM.

---

## Database: _PostgreSQL_

The database I will be using to house all my data is the open source RDMS project _PostgreSQL_. The installation process creates the user `postgres`. Log into the default database as that user and create a password.

```bash
$ sudo -u postgres psql postgres
```

```sql
postgres=# ALTER USER postgres WITH ENCRYPTED PASSWORD 'some password';
postgres=# \q
```

`\q` quits _psql_, which is the open-source client you're using to access the database. You then want to edit the following line in `/etc/postgresql/11/main/pg_hba.conf`...

```bash
...
# Database administrative login by Unix domain socket
local	all		postgres					peer
...
```

... to make it say...

```bash
...
# Database administrative login by Unix domain socket
local	all		postgres						md5
...
```

Restart the database.

```bash
$ sudo /etc/init.d/postgresql restart
```

Try login in to the database via `psql -U postgres`. It should now prompt you for a password. Now we're going to create another user with the same username as your login with much of the same priveleges.

```bash
$ sudo createuser -U postgres -d -E -l -P -r -s new_username
```

It will ask you for the password for `new_username` twice, and then it will ask you for the password for user `postgres`. Again, change the `pg_hba.conf` file to ask for the password upon entry. Change it from...

```bash
# "local" is for Unix domain socket connections only
local	all		all								peer
```

...to the following:

```bash
# "local" is for Unix domain socket connections only
local	all		all								md5
```

Restart it again, and try logging into the default database via `psql postgres` without the default user. Your username should now work and after entering in your password. Now head back to `pg_hba.conf` so we can configure remote connections. Change the `IPv4` and `IPv6` sections to match the following:

```bash
# IPv4 local connections:
host	all		all				127.0.0.1		md5
host	all		all				0.0.0.0/0		md5		# add this line
# IPv6 local connections:
host	all		all				::1/128			md5
host	all		all				::/0			md5
```

You're also going to edit the file `postgresql.conf` in the same directory. Scroll down and uncomment the line `listen_addresses = 'localhost'`. Also, change `'localhost'` to `'*'`, so it reads `listen_addresses = '*'`. This will allow _PostgreSQL_ to listen on all ports. I also changed the default port to something non-standard while were here to 8008 to help cut down on authorization attempts. Before leaving, I want to log back into _PostreSQL_ and create a database for all my project's data via `CREATE DATABASE [database_name];`. You should know be able to access it remotely from your local computer via `psql -h [host_address] -p [port_number] -U [username] -d [database_name]`. The host address can either be the public IP address or a domain name if you've assigned one. The rest of those options can be filled in with the configuration we've set up above. You can just as well ignore this last part implementing remote access. I wanted to in order to work with my database locally; however, if you just want to continually ssh into your server, that's fine as well. In the end, I'll remove remote access as a security precaution.

---

## _Flask_ 

_Flask_ is a micro-framework written in _Python_ for the purpose of developing web applications. I want to now start setting up my _Flask_ app, but not before I implement a web server on my compute engine. My web page uses _Apache_ and a _uWGI_ module; however, I wanted to try another web server, so I set my eyes set on _Nginx_ with _Gunicorn_ as my middleware. _Nginx_ is designed for high concurrency and is able to handle a great deal more connections. _Gunicorn_ is an application interface our _Nginx_ will use to communicate with our app. I found a [tutorial](https://medium.com/faun/deploy-flask-app-with-nginx-using-gunicorn-7fda4f50066a) on _Medium_ that was incredibly straight forward. It helped me get set up in a few minutes. 

First, I want to get set up with all my dependencies outside of _Python_. _Debian Buster_ should already comes with 3.8 at the time of writing. When I installed _virtualenv_, it defaulted to using Python 2.7; however, I want to use the version included with _Buster_. If you're following suit, remember to specify `-p python3` when creating your virtual environment. Activate your environment and install the python dependencies `flask ` and `gunicorn`. `tree` is a convenient utility that will display the contents of a directory in a tree-like format. This will be more to illustrate the structure of the project as it moves forward.

```bash
$ sudo apt install virtualenv nginx tree
$ mkdir portfolio && cd portfolio
$ virtualenv venv -p python3
$ source venv/bin/activate
(venv) $ pip install gunicorn flask
```

Go ahead and create a sample _Flask_ app. Again, this is just to get things set up. For now, I'm using the default _Hello World!_ app that everyone always starts with.

<iframe src="https://codekeep.io/embed/?sid=app-py" width=100% height=275></iframe>

Most tutorials would then have you view it by running `python app.py`. If you do the same, you won't be able to view it since this a local instance. For now, let's move on to our WSGI entry point.

<iframe src="https://codekeep.io/embed/?sid=wsgi-py" width=100% height=200> </iframe>

if you look at the structure of our demo project so far, there should only be the two files and a virtual environment directory.

```
portfolio/
├── app.py
├── venv
└── wsgi.py
```

Now we need to set up a service using `systemd` so our app will be served whenever the server boots.

```bash
$ cd /etc/systemd/system
$ vim app.service
```

Add the following unit file:

<iframe src="https://codekeep.io/embed/?sid=app-service" width=100% height=375> </iframe>

Now, start and enable the service.

```bash
$ sudo systemctl start app
$ sudo systemctl enable app
```

Now, we need to set up _Nginx_. There are two folders under `/etc/nginx`: `sites-available` and `sites-enabled`. The first will contain the config files for all of your projects. `sites-enabled` will contain the config files that are _actively_ read and run. _Apache_  web server functions the same way. Create a config file for your app under `sites-available` via `sudo vim /etc/nginx/sites-available/portfolio`.

<iframe src="https://codekeep.io/embed/?sid=portfolio" width=100% height=275> </iframe>

Once you've created a config file, you're going to create a symbolic link to it in the `sites-enabled` directory via `sudo ln -s /etc/nginx/sites-available/portfolio ../sites-enabled/portfolio`. Make sure you do the full path for the symbolic link source. Test for any syntax errors with `sudo nginx -t`. If none, restart the servce via `sudo systemctl restart nginx.`

We need to open up our ports before we can access anything. I prefer to try and do things through the command line; however, I'm not to sure on how it interacts with Google's VM rules. In this case, I just dealt with the GCP Console to change the rules. Under  GCP > Resources, it should list your compute engine. Click on it to go to a list of your instances. Click the name to go to the _VM instance details_, then click _Edit_ up at the top. There should be a small section for _Firewalls_ if you scroll down that reads "Allow HTTP traffic" and "Allow HTTPS traffic". You can create other rules if you explore networking a little more. For now, make sure these are both checked off and save the changes. These will open up ports 80 and 443, respectively, and allow incoming requests for your web page.

Now everything is done. If you go to your external IP address of your instance listed under _VM instance details_, you will be greeted by your app and the text "Hello World!" 

That's it for now. We have our example _Vue_ page via _npm_ integrated with _Firebase_ and our _PostgreSQL_ database and _Flask_ app set up on a _Google_ compute instance. This read is already long enough, so I'll end things here. I'm going to make additional posts refining each of these pieces towards something that resembles a portfolio. _Vue_ will be its own beast. I'll also have another one regarding _Flask_ + RESTful API + _PostgreSQL_.
