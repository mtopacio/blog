---
title: "LVM"
date: 2020-09-20T10:08:27-07:00
author: Mark Topacio
categories: ["lvm","Linux"]
draft: true 
---

My daily setup is built on top LVM.

First step is to find the disk you want to work with by either running `lsblk` or `fdisk -l`. In my case, I'm using a new NVMe drive which can be found on `/dev/nvme0n1`.

Partition the disk via `gdisk /dev/nvme0n1`. 'n' starts the process for creating a new partition. I'm going to create two partitions. One for UEFI sized at 512M. The other one is for the LVM and takes up the rest of the disk. `w` writes the changes to disk. Confirm to continue.

1 512M EFI
2 100% Linux LVM

If I enter `lsblk` again, I can see my two new partitions `/dev/nvme0n1p1` and `/dev/nvme0n1p2`. I now want to establish these as physical volumes to build into logical volumes. Remember, I'll only be using the second partition, which we created as a Linux LVM. Create a physical volume with your new partition.
pvcreate /dev/nvme0n1p2
pvdisplay

Now I want to create a volume group with the new drive. Create a volume group.
vgcreate vg00 /dev/nvme0n1p2
vgdisplay

Add your logical volumes. These will have an additional step to assign your filesystem
lvcreate -L 50G -n root-lv vg00
lvcreate -L 16G -n swap-lv vg00 
lvcreate -l 100%FREE -n home-lv vg00

If I enter 'lsblk' now, I can now see that `nvme0n1p2` has been split into 3 segments with type `lvm`. I now need to format each of these volumes, including the first EFI partition.

mkfs.fat -F32 /dev/nvme0n1p1
mkfs.ext4 /dev/vg00/lv-root
mkfs.ext4 /dev/vg00/lv-home
mkswap /dev/vg00/lv-swap
swapon /dev/vg00/lv-swap

mount /dev/vg00/lv-root /mnt
mkdir /mnt/home
mkdir /mnt/boot
mount /dev/vg00/lv-home /mnt/home
mount /dev/nvme0n1p1 /mnt/boot

pacman-key --init
pacman-key --populate archlinux
pacstrap /mnt base linux linux-firmware amd-ucode vim lvm2

I was using an old live USB to install Arch, so I ran into errors with they keyring. If this happens to you, run `pacman -Sy` to update your databases and install the new keyring via `pacman -s archlinux-keyring`.

After making all the necessary installations, generate your `fstab` via `genfstab -U /mnt >> /mnt/etc/fstab`.

Logical volumes make it easier for memory management. If I have a new drive, instead of mounting it as its own directory, I could just increase the space in one of the logical volumes (i.e. lv-home). To do this I would first add it as a physical drive via `pvcreate` similar to above, then I wouls use `vgextend` and `lvextend`

vgextend vg00 /dev/newdrive
lvextend -l 100%FREE /dev/vg00/lv-home

---

# Setup

Setup the appropriate timezone. `timedatectl list-timezones` will give you a list of cities. Create a link to `/etc/localtime` to set the timezone. Sync your hardware clock to your system clock.

ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
hwclock --systohc

Uncomment your locale in `/etc/locale.gen`. In my case, I uncommented `en_US.UTF-8 UTF-8`. Generate your locales via `locale-gen`.
Name your machine by editing `/etc/hostname`
I also edited my `/etc/hosts` file by adding the following:
127.0.0.1   localhost
::1         localhost
Enter `passwd` and change your root password.

Installing some additional packages;
pacman -S grub efibootmgr base-devel networkmanager network-manager-app git openssh mtools dosfstools linux-headers sudo

edit `/etc/mkinitcpio.conf`
add `lvm2` hook
mkinitcpio -c /etc/mkinitcpio.conf -g /boot/initramfs.img -k 5.10.4-arch2-1
Enter `ls /usr/lib/modules` to determine the kernel you're working with.

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager

### New user
useradd -mG wheel [username]

change sudo settings to include users in group `wheel`.
EDITOR=vim visudo
uncoment the line towards the bottow that reads `%wheel ALL=(ALL) ALL`

When I went to reboot, something went wront. It would boot straight into a GRUB command shell. 
Use 'insmod linux' to be able to use familiar Linux commands. I tried setting the root to my boot partition, but that didn't work as well.
The error I got was telling me a filesystem hadn't been set. 

What I ended up doing was manually adding an entry via `efibootmgr`. What I entered was the following:
```
$ efibootmgr --disk /dev/nvme0n1 --part 1 --create --label "Arch Linux" --loader /vmlinuz-linux --unicode 'root=UUID=[the UUID of your root partition] rw initrd=\initramfs-linux.img' --verbose
```

Alright, let me explain that line. The manager is going to instruct my motherboard to boot from the appropriate entries. It's adding an entry to find all this info on partition one of my NVME card (i.e. /dev/nvme0n1). I'll name it "Arch Linux" to be direct, and I'll point the root directory to my the logical drive we created in the beginning (i.e. /dev/vg00/lv-root). Preferably, use the UUID which you can find with `blkid`. I'm also going to direct the loader to my init file `initramfs-linux.img`. 

After a reboot, 'Arch Linux' was listed by my motherboard. I set it as my primary, restarted again, and was greeted with the login screen after letting everything load.
