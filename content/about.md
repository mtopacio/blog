---
title: "About"
date: 2020-08-29T11:35:24-07:00
author: "Mark Topacio"
aliases: ["about","bio"]
hideComments: "true"
hideSuggestions: "true"
hideMetadata: "true"

---

This whole thing is a way of documenting a few things I'm interested in. There's no set standard or schedule. The only consistent thing is it involves some programming and relying on a few FOSS solutions. It's more of a way to reference what I've done in the past in the event I need to replicate it in the future. The process used to be done with a git server; however, I migrated over to static site generator for the aesthetics, and I wanted to include a little more that just code peppered with comments. 

Documenting all the programming concepts I've learned would help retain it and maybe help a few others. Topics includes machine learning, Python, R, bash, third-party apis. IoT, networking, etc. 

Photos are either my own or from _Unsplash_. The latter of which offers high-resolution images for free.

#### Homelab

I've built a small setup with the hopes of one day evolving it into a serious rack. Until then, it's mostly made up of an older build supported by a series of Raspberry Pi's. The whole thing started with converting an old $200 computer into a RAID set up with remote use as the primary goal. _Plex Media Server_ pushed it further, and [r/homelab](https://www.reddit.com/r/homelab/) and [r/selfhosted](https://www.reddit.com/r/selfhosted/) cemented the interest in trying to take control over my data. Now, in addition to _Plex_, it supports a _Nextcloud_ and _PostgreSQL_ server. My server runs on Debian 10 Buster, my main on Arch, and the Pi's on Raspbian. 

For those of you who somehow landed here looking for a help, hopefully these reads will lead to a little inspiration.


Mark
