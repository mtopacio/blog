import concurrent.futures
import requests
import tabula
import pandas

url_base =  "https://ark-funds.com/wp-content/fundsiteliterature/holdings/"

etfs = {"ARKW":"ARK_NEXT_GENERATION_INTERNET_ETF_ARKW_HOLDINGS.pdf",
        "ARKG":"ARK_GENOMIC_REVOLUTION_MULTISECTOR_ETF_ARKG_HOLDINGS.pdf",
        "ARKF":"ARK_FINTECH_INNOVATION_ETF_ARKF_HOLDINGS.pdf",
        "ARKK":"ARK_INNOVATION_ETF_ARKK_HOLDINGS.pdf",
        "ARKQ":"ARK_AUTONOMOUS_TECHNOLOGY_&_ROBOTICS_ETF_ARKQ_HOLDINGS.pdf"}

def thread_function(etf):

    full_url = f"{url_base}{etf}"

    response = requests.get(full_url)
    with open(etf, 'wb') as output_file:
        output_file.write(response.content)

    # read column data
    df = tabula.read_pdf(etf, silent=True, pages=1)[0]
    df = df.dropna(axis=1, how="all")

    # header gets messed up
    header = ["rank","company","ticker","cusip","shares","market_value","weight_pct"] 
    df.columns = header
    df = df.drop(["rank"], axis=1)
    print(df)

with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
    executor.map(thread_function, etfs.values())

for etf in etfs.values():

    header = ["rank","company","ticker","cusip","shares","market_value","weight_pct"]

    df = tabula.read_pdf(etf, silent=True)[0]
    df = df.dropna(axis=1, how="all")
    df.columns = header
    #print(df) 
