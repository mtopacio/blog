import psycopg2 as psql
import os
import configparser
import pandas.io.sql as pandasql

class postgres:

    def __init__(self, config):

        self.config = config

        self.conn = psql.connect(dbname=config['DATABASE']['NAME'],
                user = config['DATABASE']['USER'],
                password = config['DATABASE']['PASS'],
                host = config['DATABASE']['HOST'],
                port = config['DATABASE']['PORT'])

        self.curs = self.conn.cursor()

    def version(self):
        statement = 'SELECT version();'
        self.curs.execute(statement)
        return self.curs.fetchone()[0]

    def tables(self):
        statement = """ SELECT table_name
                        FROM information_schema.tables
                        WHERE table_schema='public'"""
        self.curs.execute(statement)
        return set(t[0] for t in self.curs.fetchall())

    def create_tables(self):
        with open('schema.sql','r') as schema_file:
            self.curs.execute(schema_file.read())
        self.conn.commit()

    def close(self):
        self.curs.close()
        self.conn.close()

if __name__=="__main__":

    config = configparser.ConfigParser()
    config.read('config.ini')
    p = postgres(config)
    print(p.version())

    if len(p.tables()) != 2:
        p.create_tables()



    p.close()
