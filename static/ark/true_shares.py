import concurrent.futures
import requests
import tabula
import pandas

def scrape():

    url_base = "https://www.true-shares.com/download-holding-usbanks?fund="

    etfs = ("LRNZ", "ECOZ")

    def thread_function(etf):

        full_url = f"{url_base}{etf}"

        response = requests.get(full_url)
        with open(etf, 'wb') as output_file:
            output_file.write(response.content)

        '''
        # header gets messed up
        header = ["rank","company","ticker","cusip","shares","market_value","weight_pct"] 

        # read column data
        df = tabula.read_pdf(etf, silent=True, pages=1)[0]
        df = df.dropna(axis=1, how="all")
        df.columns = header
        print(df)
        '''

        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
           executor.map(thread_function, etfs)


if __name__=="__main__":
    scrape()
