# Intro to Bash 2
# https://devhints.io/bash

# arithmetic
$((1 +1 ))

&&  # chaining
>   # redirection
|   # piping
>>  # 

# braceexpansion
echo {1,2,3}.txt
echo {1...5}.txt

# if-else
if [ #condition ]; then
    # do something
else
    # do another task
fi

# for loops
for i in {1..10..2}; do
    echo $i
done

# while loops
while [ #condition ]; do
    echo $myVar
    myVar=$(( $myVar + 1 ))
done

# case statements

case $variable in
    1) echo "User selected 1";;
    2) echo "User selected 2";;
    3) echo "User selected 3";;
    4) echo "User selected 4";;
esac

