# These commands were run in the asciinema videos
# This type of script is more of what I would be typing
# when recording.

# Section 1: Familiarizing your self with where you are

ls
ls -alh
ls -R
ls some_directory
# wildcards
ls *
ls file?.txt
ls file[1,2].txt

pwd

cp file1.txt some_directory/
ls some_directory

mv file2.txt some_directory
cd some_directory/
mv file2.txt ..
cd ..

rm some_directory/file1.txt
rm some_directory/file1.txt
ls

mkdir another_dir
rmdir another_dir

touch new_file.txt

whoami
uname -a

# Section 2: Exploring, Piping, and Redirecting

cat file1.txt
cat file2.txt
cat file[1,2].txt

wget https://www.w3.org/TR/PNG/iso_8859-1.txt
cat iso_8859-1.txt

head iso
head -n 5 iso
tail iso

lsd*.txt
cat file1.txt
cat file1.txt > redirect_example.txt
cat redirect_example.txt
cat file2.txt > redirect_example.txt
cat redirect_example.txt
cat file1.txt >> redirect_example.txt
cat redirect_example.txt

cat *.txt | grep file

cat file1.txt && cat file2.txt

echo test > test.txt

file test.txt
file iso
file new_file.txt

cat iso_8859-1.txt | wc
cat iso_8859-1.txt | wc -l


# Section 3: Other frequent commands 

less iso

history

ls
tar -czvf archive.tar.gz *.txt
mv archive.tar.gz some_directory && cd some_directory
pwd
ls
tar -xvf archive.tar.gz
ls
cd ..

touch chmod_example.txt
ls -l
chmod g=rw chmod_example.txt
ls -l
chmod g=-r-w,u=-r chmod_example.txt
ls
rm chmod_example.txt

ping google.com

curl https://www.w3.org/TR/PNG/iso_8859-1.txt

df -h

clear

echo this will be the command executed
!!
history (get #)
!#

# Now for the keystrokes
# Ctrl+L
Ctrl + L or clear # clears screen
# Ctrl+C and Ctrl+Z used with 'ping google.com'

Ctrl + C # cancels out of the current process
Ctrl + Z # pauses out of the current process
fg ping

# Ctrl+U
this is some long command
# Ctrl+Y

# Ctrl+A to move to the beginning a
# Ctrl+E to move to the end
this is another long command

# tab completgion
cat file1.
cat file





