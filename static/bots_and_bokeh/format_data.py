#!/usr/bin/env python3

from datetime import datetime, timedelta

with open('anomalies.txt','r') as input_file:
    lines = [line.strip() for line in input_file.readlines()]

for line in lines[10:]:

    line = [lin for lin in line.split(' ') if lin != '']

    if line[3] == 'Metis':
        # formatting for lines from the sshd log files
        ip_address = line[-3]
        timestamp = " ".join(line[:3])
        desc = " ".join(line[5:])
        dt = datetime.strptime(timestamp, "%b %d %H:%M:%S")
        dt = dt.replace(year=2020)

    else:
        # formatting for lines from the apache2 log files
        ip_address = line[0]
        timestamp = line[3][1:]
        desc = " ".join(line[5:])
        dt = datetime.strptime(timestamp, "%d/%b/%Y:%H:%M:%S") - timedelta(hours=7)

    # remove any ',' in the description. It'll interfere with anything trying to 
    # read a csv

    desc = desc.replace(',','')

    print(f"{ip_address},{dt},{desc}")
