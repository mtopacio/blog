from bokeh.io import output_file, show
from bokeh.models import ColumnDataSource
from bokeh.models.tools import HoverTool
from bokeh.plotting import figure
from bokeh.tile_providers import get_provider
import pandas as pd
import numpy as np

# name the output file
output_file("map.html")

# read in the data
df = pd.read_csv('geo_ips.csv')

# create subset of the data
df = df.drop_duplicates(subset="ip_address")[['ip_address','country','latitude','longitude']]
# add mercator coordinates
k = 6378137
df['x'] = df['longitude'] * (k * np.pi/180)
df['y'] = np.log(np.tan((90 + df['latitude']) * np.pi/360)) * k 
source = ColumnDataSource(df)

# grab tiles from provider - OpenStreetMap
tile_provider = get_provider('OSM')

# creating the range for the default view of the map
diff = (max(df['x'])-min(df['x']))*0.1
x_range,y_range = ((min(df['x'])-diff,max(df['x'])+diff), (min(df['y']),max(df['y'])))

# formatting hovering text
hover = HoverTool()
hover.tooltips = [("IP","@ip_address")]
hover.mode = "mouse"

# creating the figure
p = figure( tools='pan, wheel_zoom',
            height=600,
            width=1000,
            x_range=x_range,
            y_range=y_range,
            x_axis_type="mercator",
            y_axis_type="mercator",
            x_axis_label="Latitude",
            y_axis_label="Longitude")

p.title.text = "Unauthorized Access Attempts"
p.title.align = "center"
p.title.text_font_size = "18px"

p.add_tile(tile_provider)
p.add_tools(hover)

# adding glyphs for each unique ip address
p.circle(   x='x',
            y='y',
            source=source,
            fill_color='navy',
            line_color='navy',
            size=9,
            alpha=0.3)

show(p)
