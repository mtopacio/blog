import concurrent.futures
import requests
import json

with open("anomalies.csv", "r") as input_file:
    lines = input_file.read().split("\n")[:-1]

url = "https://freegeoip.app/json"

headers = {
    "accept":"application/json",
    "content-type":"application/json"
        }

temp = ["ip_address,date,description,country,latitude,longitude"]

def thread_function(line):

    info = line.split(",")
    ip = info[0]

    response = requests.request("GET", f"{url}/{ip}", headers=headers)
    response = json.loads(response.text)

    temp.append(f"{line},{response['country_name']},{response['latitude']},{response['longitude']}")

with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
    executor.map(thread_function, lines)

[print(t) for t in temp]



