from bokeh.io import output_file, show
from bokeh.models import ColumnDataSource
from bokeh.models.tools import HoverTool
from bokeh.plotting import figure
from bokeh.palettes import cividis
from bokeh.transform import factor_cmap
from bokeh.resources import CDN
from bokeh.embed import file_html
import pandas as pd

# name the output file
output_file("histogram.html")

# read in the data
df = pd.read_csv('geo_ips.csv')

# format columns for `ColumnDataSource` 
hist_df = df.drop_duplicates(subset="ip_address")
country_count = pd.DataFrame({"count":hist_df["country"].value_counts()})
country_count["country"] = country_count.index
source = ColumnDataSource(country_count)

# format the text when you cursor floats within the plot above your bar
hover = HoverTool()
hover.tooltips = [("Different IPs","@count (@country)")]
hover.mode = 'vline'

# mapping one of Bokeh's palletes to your factors (countries)
color_map = factor_cmap(field_name="country", palette=cividis(len(country_count.index)), factors=country_count.index.tolist())

# creating/formatting the historgram
p = figure( x_range=country_count.index.tolist(),
            y_axis_label = "Number of Unique IPs",
            x_axis_label = "Country",
            plot_height=600,
            plot_width=800,
            toolbar_location=None)
p.add_tools(hover)
p.xaxis.major_label_orientation = "vertical"
p.xgrid.visible = False
p.background_fill_color = "gray"
p.background_fill_alpha = 0.1

p.toolbar.active_drag = None
p.toolbar.active_scroll = None
p.toolbar.active_tap = None

# format main title
p.title.text = "Unauthorized Access Attempts"
p.title.align = "center"
p.title.text_font_size = "18px"

# add the vertical bars for the histogram
p.vbar(x='country',top='count', source=source, width=0.6, color=color_map)

show(p)
html = file_html(p, CDN, "Histogram")
with open("histogram.html",'w+') as output_file:
    output_file.write(html)
