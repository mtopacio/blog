#!/usr/bin/env bash

LOG_DIR=/var/log

# ssh attempts

ACCESS_FILES=$(ls ${LOG_DIR}/auth.log*)

for FILE in ${ACCESS_FILES[@]}; do

    if [[ ${FILE} == *.gz ]]; then
        gunzip -c ${FILE} | grep "sshd" | grep -Ev "mtopacio|disconnect"
    else
        cat ${FILE} | grep "sshd" | grep -Ev "mtopacio|disconnect"
    fi
done

# webserver requests

LOG_DIR=${LOG_DIR}/apache2

ACCESS_FILES=$(ls ${LOG_DIR}/access.log*)
ERROR_CODES=(400 401 403 404 405 500)

for FILE in ${ACCESS_FILES[@]}; do 
    for ERROR in ${ERROR_CODES[@]}; do
        if [[ ${FILE} == *.gz ]]; then
            gunzip -c ${FILE} | grep -Ev "marktopac.io|git|Nextcloud" | grep ${ERROR} 
        else
            cat ${FILE} | grep -Ev "marktopac.io|git|Nextcloud" | grep ${ERROR}
        fi
    done
done
